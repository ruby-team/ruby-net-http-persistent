Source: ruby-net-http-persistent
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Mohammed Bilal <mdbilal@disroot.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               rake,
               ruby-connection-pool (>= 2.2)
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-net-http-persistent.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-net-http-persistent
Homepage: https://github.com/drbrain/net-http-persistent
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: ruby-net-http-persistent
Architecture: all
Breaks: ruby-connection-pool (<< 2.4.1~)
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: Manages persistent connections using Net::HTTP
 This package provides persistent connections to Net::HTTP.
 .
 Using persistent HTTP connections can dramatically increase the speed of HTTP.
 Creating a new HTTP connection for every request involves an extra TCP
 round-trip and causes TCP congestion avoidance negotiation to start over.
 .
 Net::HTTP supports persistent connections with some API methods but does not
 make setting up a single persistent connection or managing multiple 
 connections easy.  Net::HTTP::Persistent wraps Net::HTTP and allows you to
 focus on how to make HTTP requests.
